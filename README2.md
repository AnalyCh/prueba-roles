# Índice
1. [Guest ](#guest)
2. [Reporter ](#reporter)
3. [Developer ](#developer)
4. [Mantainer ](#mantainer)


# Revisión de permisos
## Guest
1. Archivos, ramas y Merge request.
* No se puede crear una rama nueva desde Gitlab.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/2a626e76fc665f5e9a7ed9b5f15c2c6b/image.png)

* No se puede crear una rama remota desde Gitlab pero si una local

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/33ec03cc2b6c0510b30ebb2e4c58130f/image.png)
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/3942be448cb4c5bffec09bccee045d2d/image.png)
* No se puede añadir archivos

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/c377153606abaa1b1df9e03a507b363a/image.png)

* Un merge solo se realiza localmente
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/38ab4e541a4d32668c915e1a877731de/image.png)

2. Issues, labels.
* Se puede realizar issues, pero únicamente con titulo y descripción.
* No se pueden crear labels.
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/84a9384f2ec948a3be74f03ce4bde072/image.png)
* Se pueden cerrar issues

3. Milestones.
* No se puede crear un milestone o modificar uno existente. Únicamente se puede observar.
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/5c334b47997698ce2525e81c7d1f67ca/image.png)

4. Otros.
* Solo se pueden hacer issues de sugerencias para la wiki.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/5064c1459e39c769365f11b909eba41d/image.png)
* Solo se puede ver los  miembros del proyecto.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/bebfc016cbc2f8fd77cd7aeef41fc0a9/image.png)
* No se puede incluir el time tracking

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/6c73b69cb0d1e6ef4c084e35a4342509/image.png)

---
## Reporter
1. Archivos, ramas y Merge request.
* No se pueden crear ramas desde Gitlab.
* Puede crear nuevos archivos pero no se incorporan sin un merge
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/7912c5839be1460115ecda84512d03d9/image.png)
* Se puede crear un merge para archivos.
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/a735088554d33f848ac46bd7ce4246e2/image.png)
* No se puede crear una rama desde gitKraken

* No se pueden realizar Merge request

2. Issues, labels.
* Se puede crear un issue con sus labels y fechas
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/fcac068519c835bc8820041738cce88c/image.png)
* Se puede añadir el time tracking
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/27eff3a8053b72ee414ea5e53c1402bb/image.png)

3. Milestones.
* Solo se pueden ver
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/3d48f267909e73c66a20e2a1bb34264f/image.png)

4. Otros.
* Solo se puede ver a los otros miembros
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/37441c4d33611db4b09c5cdf60dbf903/image.png)

---
## Developer
1. Archivos, ramas y Merge request.
* Si se pueden crear ramas desde Gitlab.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/2227b50f68d41f42a7f2c2d0a5a1967b/image.png)
* Puede crear nuevos archivos.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/6abfe4221a7a8aeabe19889caa34d7fb/image.png)
* Se puede crear un merge para archivos.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/a735088554d33f848ac46bd7ce4246e2/image.png)
* Si se puede crear una rama desde gitKraken

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/c0d8456045a46742beed8c0d687ed7d8/image.png)
* No se puede realizar un merge a la master.
* Si se pueden realizar Merge request

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/39642850fdb38ac7bc9c7fc4e21db24f/image.png)

2. Issues, labels.
* Se puede crear un issue con sus labels y fechas, asignaciones, milestone, etc.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/787d1cdbb9b7d6119d46ffe151f99a5c/image.png)
* Se puede añadir el time tracking

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/27eff3a8053b72ee414ea5e53c1402bb/image.png)
* Se pueden crear labels

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/1ccd850dbcc8bcdf07b19a730e4424f4/image.png)

3. Milestones.
* Se pueden crear y visualizar
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/69e95e8eebb6e868c05cfb3e99e783d5/image.png)

4. Otros.
* Solo se puede ver a los otros miembros
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/89377a7f6b8794512fbbabc5e3223546/image.png)

---
## Mantainer
1. Archivos, ramas y Merge request.
* Se pueden crear archivos
* Se puede crear nuevas ramas

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/0eab58d47cd625d87d6c121632e03dd6/image.png)
* Se puede realizar y aceptar el merge request

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/f74ae9f322a7c28bdfa2227fc9c85a78/image.png)
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/b4571a6cb2aa73f7942ea2db8b59c26a/image.png)
 * Se puede realizar merge a la rama master

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/395a858c82a565e3ba3549e4ab88a823/image.png)

2. Issues, labels.
* Se pueden crear issues con todas sus características.
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/41a04cd47101395be2a63bfcc5f126aa/image.png)
* Se pueden crear y editar labels.

![image](https://gitlab.com/jossued/privilegios-prueba/uploads/ece48ec43da93cd067cbc5abe5ae2e07/image.png)

3. Milestones.
* Se puede crear y editar los milestones
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/61ba1214a798b335eeab033d7334d5aa/image.png)

4. Otros.
* Se pueden guardar entradas en la wiki
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/354bb00db9f14cca54ac8d8457d0b184/image.png)
![image](https://gitlab.com/jossued/privilegios-prueba/uploads/aa1c92a64fddb33a7ccaa66532a897c8/image.png)
